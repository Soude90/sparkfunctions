import setuptools

setuptools.setup(
	name = 'pysparkfunctions',
	version = '0.0.1',
	author = 'Soumyadeep Barman',
	author_email = 'soumronaldo09@gmail.com',
        description = 'This is a pyspark sql functions package',
        packages = setuptools.find_packages(),
        classifiers = [
        'Programming Language:: Python :: 3']
	)